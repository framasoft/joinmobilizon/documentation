# About Mobilizon

<p align="center">
    <a href="https://joinmobilizon.org">
        <img src="https://mobilizon.org/img/rose.jpg" alt="Mobilizon">
    </a>
</p>

Mobilizon is your federated organization and mobilization platform. Gather people with a convivial, ethical, and emancipating tool.

## Introduction

Mobilizon is a tool designed to create platforms for managing communities and events. Its purpose is to help as many people as possible to free themselves from Facebook groups and events, from Meetup, etc.

The Mobilizon software is under a Free licence, so anyone can host a Mobilizon server, called an instance. These instances may federate with each other, so any person with an account on *ExampleMeet* will be able to register to an event created on *SpecimenEvent*.

## ✨ Features

### 👤 Identities

Do you want to separate your family gatherings from your associative activities or militant mobilizations?
You will have the power to create multiple identities from the same account, like so many social masks.

---

### 📅 Events and groups

Create your events and make sure they will appeal to everybody.
Privacy settings and participants roles are supported.
There's no lock-in, you can interact with the event without registration.

---

## Contributing

We appreciate any contribution to Mobilizon. Check our [Contributing](4. Contribute/index.md) page for more information.

## Links

### Learn more
  * 🌐 Official website: [https://mobilizon.org](https://mobilizon.org)
  * 💻 Source: [https://framagit.org/framasoft/mobilizon](https://framagit.org/framasoft/mobilizon)

### Discuss
  * 💬 Matrix: `#Mobilizon:matrix.org` [Riot](https://riot.im/app/#/room/#Mobilizon:matrix.org)
  * IRC: `#mobilizon` on [Libera Chat](https://libera.chat)
  * 🗣️ Forum: [https://framacolibri.org/c/mobilizon](https://framacolibri.org/c/mobilizon)

### Follow
  * 🐘 Mastodon: [https://framapiaf.org/@mobilizon](https://framapiaf.org/@mobilizon)
  * 🐦 Twitter [https://twitter.com/@joinmobilizon](https://twitter.com/@joinmobilizon)

Note: Most federation code comes from [Pleroma](https://pleroma.social), which is `Copyright © 2017-2019 Pleroma Authors - AGPL-3.0`
