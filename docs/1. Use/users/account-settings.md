# Your account settings

You can access your account settings by clicking your avatar on the top bar and then **My account**.

## General

### Email

You can change the email you use to connect to your account. To do so, you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click the **General** tab on left sidebar
  * enter your new email in the **New email** field
  * enter your password in the **Password** field
  * click the **Change my email** button.

!!! note
    You'll receive a confirmation email.

### Password

To change your password, you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click the **General** tab on left sidebar
  * enter your current one into the **Old password** field
  * enter your new one into the **New password** field
  * click the **Change my password** button.

### Delete account

!!! danger
    You'll lose everything. Identities, profiles, settings, events created, messages and participations will be gone forever.
    **There will be no way to recover your data**.

To delete your account you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click the **General** tab on left sidebar
  * click the **Delete my account** button
  * enter your password to confirm your action
  * click the **Delete everything** button.

## Preferences

You can choose the language of your Mobilizon interface and your timezone. To do so, you have to:

1. click your avatar on the top bar
* click **My account**
* click the **Preferences** tab on left sidebar
* change the language or/and the time zone.

## Notifications

Here, you have the choice to receive (or not) notifications. To do so, you have to:

  1. click your profile picture
  * click **My account**
  * click **Notifications** from the left sidebar

### Browser notifications

With push notifications, you can view the most important notifications of your Mobilizon account activity in your browser. You don't even have to be logged in to Mobilizon for these notifications to appear. To enable this notification system, simply click on the **<svg style="width:24px;height:24px" viewBox="0 -5 24 24">
    <path fill="currentColor" d="M6.18,15.64A2.18,2.18 0 0,1 8.36,17.82C8.36,19 7.38,20 6.18,20C5,20 4,19 4,17.82A2.18,2.18 0 0,1 6.18,15.64M4,4.44A15.56,15.56 0 0,1 19.56,20H16.73A12.73,12.73 0 0,0 4,7.27V4.44M4,10.1A9.9,9.9 0 0,1 13.9,20H11.07A7.07,7.07 0 0,0 4,12.93V10.1Z" />
</svg> Activate browser push notifications** button in the browser.

### Notification settings

Control what you want to be notified by email or/and push:

![](../../images/en/mobilizon-notifications-check-EN.png)

If you choose to receive email notifications, you can set the frequency:

  * Do not receive any mail
  * Receive one email for each activity
  * Hourly email summary
  * Daily email summary
  * Weekly email summary

!!! note
     Announcements and mentions notifications are always sent straight away.

### Participation notifications

You can choose when you want to be notified by email. To do so, you have to:

1. click your avatar on the top bar
* click **My account**
* click the **Email notifications** tab on left sidebar
* choose between:
    * Notification on the day of the event
    * Recap every week
    * Notification before the event

### Organizer notifications

As an organizer you can receive notifications for manually approved participations to an event. You can choose between:

  * Do not receive any mail
  * Receive one email per request
  * Hourly email summary
  * Daily email summary
  * Weekly email summary

### Personal feeds

 These RSS/Atom and ICS/WebCal feeds contain event data for the events for which any of your profiles is a participant or creator. You should keep these private. You can find feeds for specific profiles on each profile edition page. 
